import React, { useEffect, useState } from 'react'
import './style.css'

export default function Pomodo() {
    function millisToMinutesAndSeconds(millis) {
        var minutes = Math.floor(millis / 60000);
        var seconds = ((millis % 60000) / 1000).toFixed(0);
        return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    }
    const [time, setTime] = useState(1500000)
    const [isStart, setIsStart] = useState(false)
    const [relax, setRelax] = useState(false)
    const [relaxTime, setRelaxTime] = useState(300000)
    const [fourth, setFourth] = useState(0)
    useEffect(() => {
        let inter = null;
        if (isStart && time > 0) {
            inter = setInterval(() => {
                setTime(time => time - 1000)
            }, 1000);
        }
        else {
            setIsStart(false)
            clearInterval(inter)
        }
        if (time === 0) {
            setRelax(true)
        }
        return () => clearInterval(inter);
    }, [isStart, time])

    useEffect(() => {
        let relaxInter = null;
        if (relax && relaxTime > 0) {
            relaxInter = setInterval(() => {
                setRelaxTime(relaxTime => relaxTime - 1000)
            }, 1000)
        }
        else {
            setRelax(false)
            clearInterval(relaxInter)
        }
        if (relaxTime === 0) {
            setFourth(fourth => fourth + 1)
            setIsStart(true)
            setTime(1500000)
            if (fourth === 3) {
                setRelaxTime(1800000)
            }
            else {
                setRelaxTime(5000)
            }
        }
        return () => clearInterval(relaxInter);
    }, [relax, relaxTime])
    return (
        <>
            <div className='col-md-8 main-title mx-auto mt-3'>
                <span className='title'>
                    Pomofocus
                </span>
            </div>
            <div className='col-md-6 mx-auto main-pomodo text-center'>
                <span className='time'>
                    {millisToMinutesAndSeconds(time)}
                </span>
                <br />
                {!relax && !isStart && (
                    <button onClick={() => setIsStart(true)} className='btn btn-outline-secondary col-md-4'>Start</button>
                )}
                {!relax && isStart && (
                    <button onClick={() => setIsStart(false)} className='btn btn-outline-secondary col-md-4'>Stop</button>
                )}
                {relax && (
                    <button className='btn btn-outline-secondary col-md-4 relax-btn'>
                        {millisToMinutesAndSeconds(relaxTime)}
                    </button>
                )}
            </div>
        </>
    )
}